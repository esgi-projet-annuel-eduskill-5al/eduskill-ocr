from flask import Flask, request, jsonify
import easyocr
import os
import re
from pdf2jpg import pdf2jpg
import shutil
from concurrent.futures import ThreadPoolExecutor
from io import BytesIO
import tempfile

app = Flask(__name__)

reader = easyocr.Reader(['fr'])

def extract_text_from_image(image_path):
    result = reader.readtext(image_path)
    text = " ".join([detection[1] for detection in result])
    return text

def extract_text_from_images(image_paths):
    with ThreadPoolExecutor(max_workers=8) as executor:
        results = list(executor.map(extract_text_from_image, image_paths))
    text = " ".join(results)
    return text

def extract_email(text):
    email_pattern = r'[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}'
    emails = re.findall(email_pattern, text)
    return emails if emails else None

def extract_phone_number(text):
    phone_pattern = r'\+?\d[\d\s.-]{8,}\d'
    phone_numbers = re.findall(phone_pattern, text)
    return phone_numbers if phone_numbers else None

def extract_name(text):
    name_pattern = r'\b[A-Z][a-z]+ [A-Z][a-z]+\b'
    match = re.search(name_pattern, text)
    if match:
        return match.group(0)
    return None

def extract_address(text):
    address_keywords = ['rue', 'avenue', 'boulevard', 'place', 'impasse', 'allée', 'chemin']
    address_pattern = rf'\d{{1,5}}\s+(?:{"|".join(address_keywords)})\s+[A-Za-z0-9\s,.-]+'
    addresses = re.findall(address_pattern, text, re.IGNORECASE)
    return addresses if addresses else None

@app.route('/upload', methods=['POST'])
def upload_file():
    if 'file' not in request.files:
        return jsonify({"error": "No file part"}), 400

    file = request.files['file']

    if file.filename == '':
        return jsonify({"error": "No selected file"}), 400

    if not file.filename.lower().endswith('.pdf'):
        return jsonify({"error": "File type not supported"}), 400

    try:
        # Sauvegarder le fichier PDF temporairement
        with tempfile.NamedTemporaryFile(delete=False) as temp_pdf:
            temp_pdf.write(file.read())
            temp_pdf_path = temp_pdf.name

        # Convertir le PDF en images
        output_dir = tempfile.mkdtemp()
        result = pdf2jpg.convert_pdf2jpg(temp_pdf_path, output_dir, pages="ALL")
        os.remove(temp_pdf_path)

        image_paths = [item for item in result[0]['output_jpgfiles']]

        # Utiliser ThreadPoolExecutor pour traiter les images en parallèle
        text = extract_text_from_images(image_paths)

        # Nettoyer les fichiers d'images
        for image_path in image_paths:
            os.remove(image_path)

        # Nettoyer le répertoire de sortie
        shutil.rmtree(output_dir)

        emails = extract_email(text)
        phone_numbers = extract_phone_number(text)
        name = extract_name(text)
        addresses = extract_address(text)

        response = {
            "emails": emails,
            "phone_numbers": phone_numbers,
            "name": name,
            "addresses": addresses,
            "text": text
        }

        return jsonify(response)

    except Exception as e:
        return jsonify({"error": str(e)}), 500

if __name__ == "__main__":
    app.run(host="0.0.0.0", port=5000, debug=True)
